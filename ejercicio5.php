
<!-- 
1 crear directorio con la fecha de hoy
2 copiar un fichero de ese directorio con el sufijo modificado -->

<?php 

function crearYCopiar(){
    $fichero = "./ejercicio5.txt";
    $modificado = "./24_11_2020/ejercicio5.modificado.txt";
    if(!file_exists("./24_11_2020")){
        mkdir("./24_11_2020", 0777);
        if(!copy($fichero, $modificado)){
            echo "Error al copiar $fichero... \n";
    }
 }
    
    else {
         if(copy($fichero, $modificado)){
             echo "Archivo copiado correctamente en su nuevo directorio";
        } 
    } 
    }

crearYCopiar();
?>